"use strict";

var http = require('http');

var cluster = require('cluster');

var cpu = require('os').cpus().length;

var app = require('./app'); //Clustering process


if (process.env.NODE_ENV == "development") {
  var server = http.createServer(app);
  server.listen(process.env.PORT);
  process.exit(0);
} else if (process.env.NODE_ENV != "production") {
  var _server = http.createServer(app);

  console.log("Server is running");

  _server.listen(process.env.PORT);
} else {
  if (cluster.isMaster) {
    console.log('Master running process on pid: ' + process.pid);

    for (var i = 0; i < cpu; i++) {
      cluster.fork();
    }
  } else {
    //TesD
    var port = process.env.PORT || 3000;

    var _server2 = http.createServer(app);

    console.log('Worker running process on pid: ' + process.pid);

    _server2.listen(port);
  }
}