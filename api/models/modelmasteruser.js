'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class modelMasterUser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };

  modelMasterUser.init({
    jenis_prasarana_id:{
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false
    },
    nama:{
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'jenis_prasarana',
    freezeTableName: true,
    schema:"ref",
    timestamps: false
  });

  return modelMasterUser;
};