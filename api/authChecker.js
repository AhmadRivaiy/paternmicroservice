var jwt = require('jsonwebtoken');
var key = require('./includes/key');
const {query, body, param, validationResult, header} = require('express-validator');
exports.checkAuth = (req, res, next)=>{
    var headers = req.headers;
    if(typeof headers['authorization']!=='undefined'){
        if(headers['authorization'].search("token=")>-1){
            jwt.verify(headers['authorization'].split(";")[0].replace("token=", ""), key.token, (err, decoded)=>{
                if(!err){
                    req.user_data=decoded;
                    return next();
                }else{
                    console.log(err);
                    res.status(403).send({message:'Not Authorized'})
                }
            });
        }else{
            res.status(400).send({message:'Auth detected, but no token detected'})
        }
    }else{
        res.status(401).send({
            message:"Token Required"
        })
    }
}

exports.generateToken = (data)=>{
    return jwt.sign(data, key.token);
}