//Lib
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const authChecker = require('./authChecker');
const routerKurikulum = require('./routes/index');

router.use('/v1/kurikulum', authChecker.checkAuth, (req, res, next) => {
    next();
}, routerKurikulum);

//exports
module.exports = router;