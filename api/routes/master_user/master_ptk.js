const MasterUserService = require('../../services/MasterUserService');
const Utils = require('../../includes/utils/Utils')
const util = new Utils();
const express = require('express');
const router = express.Router();
const validator = require('./masterValidator')
const async = require('async');
const { body } = require('express-validator');

router.get('/jp/:jp', (req, res, next) => {
    var data = { jp: req.params.jp };
    MasterUserService.getAllJp(data).then(x => {
        if (x) {
            util.setSuccess(200, 'Jenis Prasarana retrieved', x);
        } else {
            util.setSuccess(200, 'Jenis Prasarana Not found');
        }
        util.send(res);
    }).catch(err => {
        var details = {
            parent: err.parent,
            name: err.name,
            message: err.message
        }
        var error = new Error("Error pada server");
        error.status = 500;
        error.data = {
            date: new Date(),
            route: req.originalUrl,
            details: details
        };
        next(error);
    });
});

// router.get('/master_ptk',(req, res, next)=>{
//     new masterModel().getAllPTK(req.user_data.npsn).then(x=>{
//         res.send({
//             data:x
//         })
//     }).catch(err=>{
//         var details = {
//             parent:err.parent,
//             name:err.name,
//             message:err.message
//         }
//         var error = new Error("Error pada server");
//         error.status = 500;
//         error.data = {
//             date:new Date(),
//             route:req.originalUrl,
//             details:details
//         };
//         next(error);
//     });
// });

// router.get('/master_ptk/matpel_terdaftar/:ptk_terdaftar_id',(req, res, next)=>{
//     new masterModel().getMatpelPTK(req.params.ptk_terdaftar_id).then(x=>{
//         if(x[0] != null){
//             res.send({
//                 data:x
//             })
//         }else{

//         res.send({
//             data:null,
//             message: 'Tidak Ada Mata Pelajaran Terdaftar Dengan Guru Terkait'
//         })
//         }
//     }).catch(err=>{
//         var details = {
//             parent:err.parent,
//             name:err.name,
//             message:err.message
//         }
//         var error = new Error("Error pada server");
//         error.status = 500;
//         error.data = {
//             date:new Date(),
//             route:req.originalUrl,
//             details:details
//         };
//         next(error);
//     });
// });

// router.put('/master_ptk', validator.validate("master_ptk_put"), validator.verify, (req, res, next)=>{
//     new masterModel().updateMasterPTK(req.body).then(x=>{
//         res.send({
//             message: 'Sukses Update!'
//         });
//     }).catch(err=>{
//         var details = {
//             parent:err.parent,
//             name:err.name,
//             message:err.message
//         }
//         var error = new Error("Error pada server");
//         error.status = 500;
//         error.data = {
//             date:new Date(),
//             route:req.originalUrl,
//             details:details
//         };
//         next(error);
//     });
// });

module.exports = router;