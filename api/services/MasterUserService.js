const database = require('../models');

class MasterUserService {
  static async getAllJp(id) {
    try {
      return await database.modelMasterUser.findOne({
        where: { jenis_prasarana_id: Number(id.jp) }
      });
    } catch (error) {
      throw error;
    }
  }
}

module.exports = MasterUserService;