//Lib
const express = require('express');
const router = express.Router();
const routerMaster = require('./master_user/master_ptk');

// const routerTest = require('./test/test');
//Middlewares list

router.use('/master_user', routerMaster);

//exports
module.exports = router;