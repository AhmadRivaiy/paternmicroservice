const {query, body, validationResult, param,header, oneOf} = require('express-validator');
const {Op} = require('sequelize');

exports.validate = (method) =>{
    switch(method){        
        case "anggota_detail":{
            return [
                param('rombel_id').notEmpty().exists().isString()
            ]
        }
        case "master_ruangan_get":{
            return [
                query('jenis_prasarana_id').isNumeric().withMessage("Jenis Prasarana Salah/Kosong!")
            ]
        }
        case "detail_ruangan_get":{
            return [
                query('id_ruang').notEmpty().exists().isString().withMessage("ID Ruang Salah/Kosong!")
            ]
        }
        case 'master_ptk_put':{
            return [
                body('nama').isString().withMessage("Mengandung Ilegal Karakter"),
                body('tanggal_lahir').isDate().withMessage("Invalid Karakter"),
                body('tempat_lahir').isLength({max:32}).withMessage("Tempat Lahir Maksimal 32 Karakter"),
                body('alamat_jalan').isLength({max:80}).withMessage("Alamat Maksimal 80 Karakter"),
                body('nama_dusun').isLength({max:60}).withMessage("Dusun/Desa Maksimal 60 Karakter"),
                body('desa_kelurahan').isLength({max:60}).withMessage("Kelurahan Maksimal 60 Karakter"),
                body('nip').isLength({max:18}).withMessage("NIP Maksimal 18 Digit"),
                body('nik').isLength({max:16}).withMessage("NIK Maksimal 16 Digit"),
                body('no_kk').isLength({max:16}).withMessage("No.KK Maksimal 16 Digit"),
                body('nuptk').isLength({max:16}).withMessage("NUPTK Maksimal 16 Digit"),
                body('nuks').isLength({max:22}).withMessage("NUKS Maksimal 22 Digit"),
                body('rt').isNumeric().withMessage("Invalid Karakter"),
                body('rw').isNumeric().withMessage("Invalid Karakter"),
                body('kode_pos').isNumeric().isLength({max:5}).withMessage("Kode Pos Maksimal 5 Digit"),
                body('npwp').isNumeric().isLength({max:15}).withMessage("NPWP Maksimal 15 Digit"),
                body('email').isLength({max:60}).withMessage("Email Maksimal 60 Digit"),
                body('no_hp').isNumeric().isLength({max:20}).withMessage("No Hp Maksimal 20 Digit/Salah"),
                body('ptk_id').notEmpty().exists().isString().withMessage("PTK Kosong!"),
            ]
        }
        case 'master_ruangan_post':{
            return [
                body('nm_ruang').isString().isLength({max:100}).withMessage("Maksimal Nama Ruang 100 Karakter"),
                body('jenis_prasarana_id').notEmpty().exists().isString().withMessage("Jenis Prasana Kosong!"),
            ]
        }
        case 'master_ruangan_put':{
            return [
                body('nm_ruang').isString().isLength({max:100}).withMessage("Maksimal Nama Ruang 100 Karakter"),
                body('lantai').isNumeric().withMessage("Lakukan Pembulatan"),
                body('panjang').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('lebar').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('kapasitas').isNumeric().withMessage("Invalid Karakter"),
                body('luas_ruang').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('luas_plester_m2').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('luas_plafon_m2').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('luas_dinding_m2').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('luas_daun_jendela_m2').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('luas_daun_pintu_m2').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('panj_kusen_m').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('luas_tutup_lantai_m2').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('panj_inst_listrik_m').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('jml_inst_listrik').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('panj_inst_air_m').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('jml_inst_air').isNumeric().withMessage("Invalid Karakter"),
                body('panj_drainase_m').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('luas_finish_struktur_m2').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('luas_finish_plafon_m2').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('luas_finish_dinding_m2').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('luas_finish_kpj_m2').isNumeric().isDecimal().withMessage("Invalid Karakter"),
                body('id_ruang').notEmpty().exists().isString().withMessage("ID Ruang Kosong!"),
            ]
        }
        case 'master_pd_put':{
            return [
                body('nisn').isNumeric().isLength({max:10}).withMessage("NISN Maksimal 10 Digit/Salah"),
                body('nama').isString().withMessage("Mengandung Ilegal Karakter"),
                body('rt').isNumeric().withMessage("Invalid Karakter"),
                body('tanggal_lahir').isDate().withMessage("Invalid Karakter"),
                body('tempat_lahir').isLength({max:32}).withMessage("Tempat Lahir Maksimal 32 Karakter"),
                body('alamat_jalan').isLength({max:80}).withMessage("Alamat Maksimal 80 Karakter"),
                body('nama_dusun').isLength({max:60}).withMessage("Dusun/Desa Maksimal 60 Karakter"),
                body('desa_kelurahan').isLength({max:60}).withMessage("Kelurahan Maksimal 60 Karakter"),
                body('nik').isNumeric().isLength({max:16}).withMessage("NIK Maksimal 16 Digit"),
                body('rw').isNumeric().withMessage("Invalid Karakter"),
                body('kode_pos').isNumeric().isLength({max:5}).withMessage("Kode Pos Maksimal 5 Digit"),
                body('nomor_telepon_seluler').isNumeric().isLength({max:20}).withMessage("No Hp Maksimal 20 Digit/Salah"),
                body('peserta_didik_id').notEmpty().exists().isString().withMessage("Peserta Didik ID Kosong!"),
            ]
        }
    }
}

exports.verify = (req, res, next) =>{
    try {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            res.status(422).json({
                errors:errors.array()
            })
            return;
        }else{
            return next();
        }
    }catch(err){
      return next(err);
    }
}