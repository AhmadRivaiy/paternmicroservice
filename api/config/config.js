require('dotenv').config();
module.exports = {

  //Using onine database
  development: {
    use_env_variable: 'postgres://postgre:Tikomdikdb2020@180.178.111.188:5432/SyncDapo2020'
  },

  // development: {
  //   database: 'database_name',
  //   username: 'database_username',
  //   password: null,
  //   host: '127.0.0.1',
  //   dialect: 'postgres'
  // },

  production: {
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    dialect: 'postgres'
  }
};